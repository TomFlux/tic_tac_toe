use std::io::{self};

// Human: X     AI: O
#[derive(Copy, Clone)]
enum Turn { X, O }
#[derive(PartialEq, Copy, Clone)]
enum Cell { X, O, Unset }

struct GameOptions {
    board : [Cell; 9], 
    turn : Turn,
}

#[derive(Clone)]
struct Node {
    value : Option<i8>,
    children : Option<Box<Vec<Node>>>,
}

impl Turn {
    fn to_cell(&self) -> Cell {
        match *self {
            Turn::X => Cell::X,
            Turn::O => Cell::O,
        }
    }

    fn next_turn(&self) -> Turn {
        match *self {
            Turn::X => Turn::O,
            Turn::O => Turn::X,
        }
    }
}

impl Cell {
    fn to_turn(&self) -> Turn {
        match *self {
            Cell::X => Turn::X,
            Cell::O => Turn::O,
            _ => panic!("Tried to convert Cell::Unset to Turn!"),
        }
    }
}

fn main() {
    let mut go = GameOptions {
        board : [Cell::Unset; 9],
        turn : Turn::X, 
    };
    print_board(&go.board);
    main_loop(&mut go);
}

fn main_loop(game_options: &mut GameOptions) {
    let mut playing : bool = true;
    while playing {
        match game_options.turn {
            Turn::X => { 
                playing = players_turn(game_options);
                game_options.turn = Turn::O; 
            }
            Turn::O => { 
                playing = ai_turn(game_options);
                game_options.turn = Turn::X; 
            }
        }
    }
}

fn players_turn(game_options : &mut GameOptions) -> bool {
    println!("\nPlease enter cell: ");
    let mut input_buffer = String::new();
    match io::stdin().read_line(&mut input_buffer) {
        Ok(_n) => {
            match input_buffer.trim().parse::<i32>() {
                Ok(cell) => {
                    match cell {
                        0..=8 => { 
                            return handle_board_change(&mut game_options.board, &game_options.turn, cell as usize); 
                        }
                        _ => { println!("Invalid input because range error"); }
                    }
                }
                Err(_error) => { println!("Invalid input because couldn't parse") }
            }
        }
        Err(error) => { println!("error: {}", error); }
    }

    true
}

fn ai_turn(game_options : &mut GameOptions) -> bool {
    println!("AI's turn");
    fake_turn(&mut game_options.board, game_options.turn);
    true
}

fn fake_turn(board : &mut [Cell; 9], turn : Turn) -> Node {
    let mut tree = Node {
        value : None,
        children : Some(Box::new(Vec::new())),
    };
    let mut found_unset = false;
    for (idx, cell) in board.iter().enumerate() {
        if cell == &Cell::Unset {
            found_unset = true;
            let mut temp_board = &mut board.clone();
            temp_board[idx] = turn.to_cell(); 
            tree
                .children
                .to_owned()
                .unwrap()
                .push(
                    fake_turn(
                       &mut temp_board, turn.next_turn() 
                    ));
        }
    }

    if !found_unset {
        match check_winner(&board, &turn) {
            "draw" => { 
                tree.value = Some(0); 
            }
            "winner" => {
                match turn {
                    Turn::X => { tree.value = Some(-1); }
                    Turn::O => { tree.value = Some(1); }
                }    
            }
            _ => {
                tree.value = Some(123);
            }
        }
    }
    tree
}

fn handle_board_change(board : &mut [Cell; 9], turn : &Turn, cell : usize) -> bool {
    if board[cell] != Cell::Unset {
        println!("Cannot go there!");
        return true;
    }

    board[cell] = turn.to_cell();
    print_board(&board);

    match check_winner(&board, &turn) {
        "draw" => { 
            println!("It's a draw!");
            return false; 
        }
        "winner" => {
            let winner = match turn {
                Turn::X => { "X" }
                Turn::O => { "O" }
            };
            println!("{}'s win!", winner);
            return false;
        }
        _ => {
            return true;
        }
    }

}

fn print_board(board : &[Cell; 9]) {
    for (idx, x) in board.iter().enumerate() {
        let glyph = match x {
            Cell::X => { String::from("❌") }
            Cell::O => { String::from("●") }
            Cell::Unset => { idx.to_string() }
        }; 

        if (idx % 3) == 0 {
            println!("-----|-----|-----");
        }
        match idx % 3 {
            0 => print!("  {}  ", glyph),
            2 => println!("  {}  ", glyph),
            _ => print!("|  {}  |", glyph)
        } 

    } 
}

fn check_winner<'a>(board : &'a [Cell; 9], turn: &'a Turn) -> &'a str {
    let curr_turn = turn.to_cell(); 

    let mut saw_unset = false;
    for x in board.iter() {
        if x == &Cell::Unset { 
            saw_unset = true;
            break;
        } 
    }

    if !saw_unset { return "draw"; }

    if board[0] == curr_turn && board[4] == curr_turn && board[8] == curr_turn { return "winner" }
    if board[2] == curr_turn && board[4] == curr_turn && board[6] == curr_turn { return "winner" }

    for x in 0..3 {
        if board[0 + (3 * x)] == curr_turn && board[1 + (3 * x)] == curr_turn && board[2 + (3 * x)] == curr_turn { return "winner"; }
        if board[0 + x] == curr_turn && board[3 + x] == curr_turn && board[6 + x] == curr_turn { return "winner"; }
    }

    return "keep playing";
}
